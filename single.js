const axios = require("axios");

const url = 'https://nodes-on-nodes-challenge.herokuapp.com/nodes/';

(async () => {
  console.log("Beginning traversal...");
  const nodeMap = new Map();
  nodeMap.set('089ef556-dfff-4ff2-9733-654645be56fe', 1);
  const remainingIds = ['089ef556-dfff-4ff2-9733-654645be56fe'];
  let apiCalls = 0;
  
  while(remainingIds.length > 0){
    apiCalls++
    const nextId = remainingIds.shift();
    const apiResponse = await axios.get(`${url.concat(nextId)}`);
    const data = apiResponse.data[0];

    data['child_node_ids'].forEach((node) => {
      if(nodeMap.has(node)){
        const currentCount = nodeMap.get(node);
        nodeMap.set(node, currentCount+1);
      } else {
        nodeMap.set(node, 1);
        remainingIds.push(node);
      }
    })
  }

  const totalUniqueNodes = nodeMap.size;
  let popularNode, popularNodeCount

  nodeMap.forEach((nodeValue, nodeKey) => {
    if(!popularNodeCount || nodeValue > popularNodeCount){
      popularNode = nodeKey;
      popularNodeCount = nodeValue;
    }
  })

  console.log(`Total number of API calls: ${apiCalls}`);
  console.log(`Total number of unique nodes: ${totalUniqueNodes}`);
  console.log(`Most common nodeID: ${popularNode}`);
})()
