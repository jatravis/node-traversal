# Node Traversal
This Node Traversal repo contains simple scripts to determine the total number of unique nodes in a tree, as well as the most common nodeID shared between nodes. These scripts can be ran in the terminal using yarn, and require you have NodeJS installed.

# Getting Started
1. Clone the repository
2. Run yarn install or npm install (the only dependency is the axios library)
3. In the terminal, run ```yarn run single``` to run the single.js script
4. In the terminal, run ```yarn run multiple``` to run the multiple.js script
