const axios = require("axios");

const url = 'https://nodes-on-nodes-challenge.herokuapp.com/nodes/';

(async () => {
  console.log("Beginning traversal...");
  const nodeMap = new Map();
  nodeMap.set('089ef556-dfff-4ff2-9733-654645be56fe', 1);
  let nextIds = '089ef556-dfff-4ff2-9733-654645be56fe';
  let apiCalls = 0;
  
  while(nextIds){
    apiCalls++
    const apiResponse = await axios.get(`${url.concat(nextIds)}`);
    const data = apiResponse.data;
    
    nextIds = '';

    data.forEach((parentNode) => {
      parentNode['child_node_ids'].forEach((nodeId) => {
        if(nodeMap.has(nodeId)){
          const currentCount = nodeMap.get(nodeId);
          nodeMap.set(nodeId, currentCount+1);
        } else {
          nodeMap.set(nodeId, 1);
          nextIds = nextIds.concat(`${nodeId},`);
        }
      })
    })

  }

  const totalUniqueNodes = nodeMap.size;
  let popularNode, popularNodeCount

  nodeMap.forEach((nodeValue, nodeKey) => {
    if(!popularNodeCount || nodeValue > popularNodeCount){
      popularNode = nodeKey;
      popularNodeCount = nodeValue;
    }
  })
  
  console.log(`Total number of API calls: ${apiCalls}`);
  console.log(`Total number of unique nodes: ${totalUniqueNodes}`);
  console.log(`Most common nodeID: ${popularNode}`);
})()
